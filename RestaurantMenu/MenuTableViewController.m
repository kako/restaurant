//
//  menuTableViewController.m
//  RestaurantMenu
//
//  Created by iosdev on 20/10/15.
//  Copyright (c) 2015 iosdev. All rights reserved.
//

#import "MenuTableViewController.h"


static NSString *SectionHeaderViewIdentifier = @"SectionHeaderViewIdentifier";
static NSString *mealsCategoryURL = @"http://papagaj-breweria.herokuapp.com/api/v1/menu/54ca39f401731406200082df/meal/category";
static NSString *addonsURL = @"http://papagaj-breweria.herokuapp.com/api/v1/menu/54ca39f401731406200082df/addon";

@interface MenuTableViewController ()

@end

@implementation MenuTableViewController 

- (BOOL)canBecomeFirstResponder {
    
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.selectedSection = -1;
    
    UINib *sectionHeaderNib = [UINib nibWithNibName:@"SectionHeaderView" bundle:nil];
    [self.tableView registerNib:sectionHeaderNib forHeaderFooterViewReuseIdentifier:SectionHeaderViewIdentifier];
    
    if(!self.categoryArray)
        [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getData{
    
    //get requests
    NSData *responseDataCategories = [self getRequest:mealsCategoryURL];
    NSData *responseDataAddons = [self getRequest:addonsURL];
    
    //data was not fetched completely
    if ((!responseDataCategories) || (!responseDataAddons))
        [self noConectionAlert];
    // data was loaded
    else{
        NSArray *categories = [self loadData:responseDataCategories forKey:@"mealCategories"];
        NSArray *addons = [self loadData:responseDataAddons forKey:@"addOns"];
        
        //extract category names and assign them to instance variable categoryArray and create array of meals ordered by categories
        self.categoryArray =[[NSMutableArray alloc] init];
        self.itemsByCategoryArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary *meal in categories){
            [self.categoryArray addObject:[meal valueForKey:@"name"]];
            [self.itemsByCategoryArray addObject:[meal valueForKey:@"meals"]];
        }
        
        //retrive unique addon category names
        NSSet *uniqueAddonCategories = [NSSet setWithArray:[addons valueForKey:@"category"]];
        self.addonsCategoryArray = [[NSArray alloc] initWithArray:[[uniqueAddonCategories allObjects] valueForKey:@"name"]];
        
        //create array of addons indexed by category
        self.addonsByCategoryArray = [[NSMutableArray alloc] init];
        NSString *nameOfCategory;
        
        for (NSString *addonCategory in self.addonsCategoryArray){
            
            //array to store addons from the same category
            NSMutableArray *addonsFromCategory = [[NSMutableArray alloc] init];
            
            
            for (NSDictionary *addon in addons){
                nameOfCategory = [[addon valueForKey:@"category"] valueForKey:@"name"];
                if ([nameOfCategory isEqualToString:addonCategory])
                    [addonsFromCategory addObject:addon];
            }
            
            //fill the array
            [self.addonsByCategoryArray addObject:[[NSMutableArray alloc] initWithArray:addonsFromCategory copyItems:YES]];
        }
    }
}

-(void)noConectionAlert{
    //this function displays connection alert
    
    UIAlertView * noDataAlert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                           message:@"Application was not able to fetch all the data from the server."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
    [noDataAlert show];
}

-(NSData *) getRequest:(NSString *)url{
    //simple get request
    
    NSURL *urlToGet = [NSURL URLWithString:url];
    
    //creating request
    NSURLRequest *request = [NSURLRequest requestWithURL:urlToGet];
    NSURLResponse *response;
    NSError *error;
    
    //retriving the data from the server
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    return responseData;
}

-(NSArray *) loadData:(NSData *) dataToLoad forKey:(NSString *) key{
    //this function loads array from response data for a specific key
    
    NSDictionary *responseJson=[NSJSONSerialization JSONObjectWithData:dataToLoad options:0 error:nil];
    
    NSArray *returnAray = [[NSArray alloc] initWithArray:[responseJson objectForKey:key]];
    return returnAray;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [self.categoryArray count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [self.categoryArray objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.selectedSection == section)
        return [self.itemsByCategoryArray[section] count];
    else
        return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    CategorySectionHeaderView *sectionHeaderView = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:SectionHeaderViewIdentifier];
    
    sectionHeaderView.titleLabel.text = self.categoryArray[section];
    sectionHeaderView.section = section;
    sectionHeaderView.delegate = self;
    
    return sectionHeaderView;
}

- (void)sectionHeaderView:(CategorySectionHeaderView *)sectionHeaderView sectionOpened:(NSInteger)sectionOpened {
    
    //store index of previosely opened section and opened section
    NSInteger previousOpenSectionIndex = self.selectedSection;
    self.selectedSection = sectionOpened;
    
    //array of inedexPaths to insert for selected section
    NSInteger countOfRowsToInsert = [self.itemsByCategoryArray[sectionOpened] count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:sectionOpened]];
    }
    
    //array of inedexPaths to delete for deselected section
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
    //the array of indexPaths to deletion is filled if there is opened section
    if (previousOpenSectionIndex != -1) {
        NSInteger countOfRowsToDelete = [self.itemsByCategoryArray[previousOpenSectionIndex] count];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
        }
    }
    
    // style the animation so that there's a smooth flow in either direction (possible issue for 2nd view)
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenSectionIndex == -1 || sectionOpened < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    //animate the changes
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.tableView endUpdates];
}

- (void)sectionHeaderView:(CategorySectionHeaderView *)sectionHeaderView sectionClosed:(NSInteger)sectionClosed {

    if (self.selectedSection == sectionClosed)
        self.selectedSection = -1;
    
    NSInteger countOfRowsToDelete = [self.tableView numberOfRowsInSection:sectionClosed];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:sectionClosed]];
        }
        [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //inicialize a cell
    MealCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SimpleTableItem"];
    
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"MyCell" bundle:nil] forCellReuseIdentifier:@"SimpleTableItem"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"SimpleTableItem"];
    }
    
    //get name size and price for cell
    NSString *nameOftheMeal = [self.itemsByCategoryArray[indexPath.section][indexPath.row] objectForKey:@"name"];
    NSDictionary *servingSize = [self.itemsByCategoryArray[indexPath.section][indexPath.row] objectForKey:@"servingSize"];
     NSString *sizeOftheMeal = [servingSize objectForKey:@"size"];

    //workaround to display only 2 decimal digits
    NSString *priceOftheMeal = [NSString stringWithFormat:@"%.02f", round(100.0f * [ [servingSize objectForKey:@"price"] floatValue]) / 100.0f];
    //[NSString stringWithFormat:@"%.02f", myFloat]
    
    //set the labels of the cell
    [cell.name setText:nameOftheMeal];
    [cell.size setText:sizeOftheMeal];
    [cell.price setText:[NSString stringWithFormat:@"%@ %@", priceOftheMeal, @"€"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    //check if there are some addons for selected meal
    if ([[self.itemsByCategoryArray[indexPath.section][indexPath.row] objectForKey:@"addOnIds"] count])
    {
        //initialize new tableview controler with proper addons array
        MenuTableViewController *newTableController = [[MenuTableViewController alloc] initWithStyle:UITableViewStylePlain];
        newTableController.categoryArray = [[NSMutableArray alloc] initWithArray:self.addonsCategoryArray];
        newTableController.itemsByCategoryArray = [[NSMutableArray alloc] initWithArray:self.addonsByCategoryArray];
        [[self navigationController] pushViewController:newTableController animated:YES];
        
      //  [self presentViewController:newTableController animated:YES completion:nil];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // no autolayout...
    return 100.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //no autolayout...
    return 40.0;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

@end
