//
//  NavViewController.m
//  RestaurantMenu
//
//  Created by iosdev on 21/10/15.
//  Copyright (c) 2015 iosdev. All rights reserved.
//

#import "NavViewController.h"

@interface NavViewController ()

@end

@implementation NavViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationBar.topItem.title = @"Menu";
    
    // I could have done the loading of data here to make TableviewController code more clean
    // Probably to use core data too , however I have no experiences with that 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
