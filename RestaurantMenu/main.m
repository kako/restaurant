//
//  main.m
//  RestaurantMenu
//
//  Created by iosdev on 20/10/15.
//  Copyright (c) 2015 iosdev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
