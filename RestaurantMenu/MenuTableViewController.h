//
//  menuTableViewController.h
//  RestaurantMenu
//
//  Created by iosdev on 20/10/15.
//  Copyright (c) 2015 iosdev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MealCell.h"
#import "CategorySectionHeaderView.h"
#import "NavViewController.h"

@interface MenuTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, SectionHeaderViewDelegate>

@property (nonatomic, strong) NSMutableArray *categoryArray;
@property (nonatomic, strong) NSMutableArray *itemsByCategoryArray;
@property (nonatomic, strong) NSArray *addonsCategoryArray;
@property (nonatomic, strong) NSMutableArray *addonsByCategoryArray;
@property (nonatomic) NSInteger selectedSection;
@property (nonatomic) IBOutlet CategorySectionHeaderView *sectionHeaderView;
@property (weak, nonatomic) IBOutlet UINavigationItem *navBar;



-(void) getData;
-(NSData *) getRequest:(NSString *)url;
-(void) noConectionAlert;
-(NSArray *) loadData:(NSData *) dataToLoad forKey:(NSString *) key;

@end
