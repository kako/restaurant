//
//  mealCell.h
//  RestaurantMenu
//
//  Created by iosdev on 20/10/15.
//  Copyright (c) 2015 iosdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MealCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *size;
@property (weak, nonatomic) IBOutlet UILabel *price;


@end
