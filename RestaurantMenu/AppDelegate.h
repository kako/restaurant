//
//  AppDelegate.h
//  RestaurantMenu
//
//  Created by iosdev on 20/10/15.
//  Copyright (c) 2015 iosdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

