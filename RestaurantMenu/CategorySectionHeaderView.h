//
//  CategorySectionView.h
//  RestaurantMenu
//
//  Created by iosdev on 20/10/15.
//  Copyright (c) 2015 iosdev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@protocol SectionHeaderViewDelegate;

@interface CategorySectionHeaderView : UITableViewHeaderFooterView

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *disclosureButton;
@property (nonatomic, weak) IBOutlet id <SectionHeaderViewDelegate> delegate;

@property (nonatomic) NSInteger section;

- (void)toggleOpenWithUserAction:(BOOL)userAction;

@end


@protocol SectionHeaderViewDelegate <NSObject>

@optional
- (void)sectionHeaderView:(CategorySectionHeaderView *)sectionHeaderView sectionOpened:(NSInteger)section;
- (void)sectionHeaderView:(CategorySectionHeaderView *)sectionHeaderView sectionClosed:(NSInteger)section;

@end
